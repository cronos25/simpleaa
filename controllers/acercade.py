# -*- coding: utf-8 -*-
@auth.requires_login()
def index():
    """
    example action using the internationalization operator T and flash
    rendered by views/admin/index.html or views/generic.html

    if you need a simple wiki simply replace the two lines below with:
    return auth.wiki()
    """
    links = [
        lambda r:IMG(_src=URL('default','download',args=r.avatar),
            _style='max-width:75px; max-height: 75px;',_class='foto_empleado'),
        lambda r:A("Editar",_href=URL('acercade','edit_employe',
            args=r.id),_class='btn btn-info')
    ]
    
    dataemploye = db(db.employe).select()
    dataset = db(db.aboutus).select()
    return dict(dataset=dataset,dataemploye=dataemploye)

@auth.requires_login()
def edit():
    record = db.aboutus(request.args(0)) or redirect(URL('acercade','index'))
    form = SQLFORM(db.aboutus,record)
    dataset = db(db.aboutus).select()
    if form.process().accepted:
        print 'Item agregado al menu'
        dataset = db(db.aboutus).select()
        response.flash = 'Item agregado al menu'
    elif form.errors:
        print 'Se encontraron errores'
        response.flash = 'Se encontraron errores'

    return dict(form=form,dataset=dataset)

@auth.requires_login()
def edit_employe():
    record = db.employe(request.args(0)) or redirect(URL('acercade','index'))
    form = SQLFORM(db.employe,record)
    dataset = db(db.employe).select()
    if form.process().accepted:
        print 'Item agregado al menu'
        dataset = db(db.employe).select()
        response.flash = 'Item agregado al menu'
    elif form.errors:
        print 'Se encontraron errores'
        response.flash = 'Se encontraron errores'

    return dict(form=form,dataset=dataset)

@auth.requires_login()
def new_employe():
    form = SQLFORM(db.employe)
    dataset = db(db.employe).select()
    if form.process().accepted:
        print 'Employe'
        dataset = db(db.employe).select()
        response.flash = 'Item agregado al menu'
    elif form.errors:
        print 'Se encontraron errores'
        response.flash = 'Se encontraron errores'

    return dict(form=form,dataset=dataset)

@auth.requires_login()
def delete_employe():
    deleted = db(db.employe.id==request.args(0)).delete()

    if deleted:
        response.flash = 'empleado eliminado eliminada'
    else:
        response.flash = 'Se encontraron errores'

    redirect(URL('acercade','index.html'))

    return dict()