# -*- coding: utf-8 -*-
@auth.requires_login()
def index():
    """
    example action using the internationalization operator T and flash
    rendered by views/admin/index.html or views/generic.html

    if you need a simple wiki simply replace the two lines below with:
    return auth.wiki()
    """
    
    dataset = db(db.empresa).select()

    return dict(dataset=dataset)
    
@auth.requires_login()
def edit():
    record = db.empresa(request.args(0)) or redirect(URL('empresa','index'))
    form = SQLFORM(db.empresa,record)
    dataset = db(db.empresa).select()
    if form.process().accepted:
        print 'Item agregado al menu'
        dataset = db(db.empresa).select()
        response.flash = 'Item agregado al menu'
    elif form.errors:
        print 'Se encontraron errores'
        response.flash = 'Se encontraron errores'

    return dict(form=form,dataset=dataset)