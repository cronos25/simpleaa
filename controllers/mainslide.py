@auth.requires_login()
def index():
    
    dataset = db(db.main_slide).select()

    return dict(dataset=dataset)

@auth.requires_login()
def new():
    form = SQLFORM(db.main_slide)
    dataset = db(db.main_slide).select()
    if form.process().accepted:
        dataset = db(db.main_slide).select()
        response.flash = 'Item agregado al menu'
    elif form.errors:
        response.flash = 'Se encontraron errores'

    return dict(form=form,dataset=dataset)

@auth.requires_login()
def edit():
    record = db.main_slide(request.args(0)) or redirect(URL('mainslide','index'))
    form = SQLFORM(db.main_slide,record)
    dataset = db(db.main_slide).select()
    if form.process().accepted:
        print 'Item agregado al menu'
        dataset = db(db.main_slide).select()
        response.flash = 'Item agregado al menu'
    elif form.errors:
        print 'Se encontraron errores'
        response.flash = 'Se encontraron errores'

    return dict(form=form,dataset=dataset)

@auth.requires_login()
def delete():
    deleted = db(db.main_slide.id==request.args(0)).delete()

    if deleted:
        response.flash = 'slide eliminado eliminada'
    else:
        response.flash = 'Se encontraron errores'

    redirect(URL('mainslide','index.html'))

    return dict()