# -*- coding: utf-8 -*-
@auth.requires_login()
def index():
    
    dataset = db(db.project).select()
    return dict(dataset=dataset)

@auth.requires_login()
def new():
    form = SQLFORM(db.project)
    dataset = db(db.project).select()
    if form.process().accepted:
        dataset = db(db.project).select()
        response.flash = 'Item agregado al menu'
    elif form.errors:
        response.flash = 'Se encontraron errores'

    return dict(form=form,dataset=dataset)
    
@auth.requires_login()
def edit():
    record = db.project(request.args(0)) or redirect(URL('proyecto','index'))
    form = SQLFORM(db.project,record)
    dataset = db(db.project).select()
    if form.process().accepted:
        print 'Item agregado al menu'
        dataset = db(db.project).select()
        response.flash = 'Item agregado al menu'
    elif form.errors:
        print 'Se encontraron errores'
        response.flash = 'Se encontraron errores'

    return dict(form=form,dataset=dataset)

@auth.requires_login()
def view():
    dataset = db.project(request.args(0)) or redirect(URL('proyecto','index'))

#dataset = db(db.project.id==request.args(0)).select()
    #if not dataset:
        #redirect(URL('proyecto','index'))

    return dict(dataset=dataset)

@auth.requires_login()
def delete():
    deleted = db(db.project.id==request.args(0)).delete()

    if deleted:
        response.flash = 'slide eliminado eliminada'
    else:
        response.flash = 'Se encontraron errores'

    redirect(URL('proyecto','index.html'))

    return dict()

