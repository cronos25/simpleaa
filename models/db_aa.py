# -*- coding: utf-8 -*-

#######################################################################
##                                                                   ##
##              Model database Autoadmin page                ##
##                                                                   ##
#######################################################################

# Fijando el session timeout en un chillion de segundos
auth.settings.expiration = 999999999

DB = db.define_table

#Locales
DB('empresa',
    Field('name',label='Nombre'),
    Field('slogan',label='Eslogan'),
    Field('email',label='Correo electronico'),
    Field('address',label='Direccion'),
    Field('phone',label='Telefono'),
    Field('facebook',label='Facebook'),
    Field('tweeter',label='Tweeter'),
    Field('gplus',label='Google plus'),
    Field('linkedin',label='Linkedin'),
    Field('instagram',label='Instagram'),
    Field('icono','upload',label='Icono'),
    Field('logocompleto','upload',label='Logo con nombre de la empresa'),
    Field('logosintipo','upload',label='Logo sin nombre'),
    auth.signature
)

#Empleados
DB('employe',
    Field('user_id','integer',unique=True),
    Field('name',label='Nombre'),
    Field('gender',requires=IS_IN_SET(['Hombre','Mujer','Otro'])),
    Field('apodo',label='apodo'),
    Field('avatar','upload',autodelete=True),
    Field('cargo',label='cargo'),
    Field('description',label='descripcion'),
    auth.signature
)

#Acerca de
DB('aboutus',
    #Field('local_nonna_id','reference local_nonna',label='Local'),
    Field('mision',label='Mision'),
    Field('valor',label='Valor'),
    Field('vision',label='Vision'),
    Field('acercade',label='Acerca de'),
    auth.signature
)

#Main slide
DB('main_slide',
    Field('slide_id','integer',unique=True),
    Field('slide_title',label='Titulo slide'),
    Field('slide_img','upload',requires=IS_IMAGE(),autodelete=True),
    Field('slide_description',label='descripcion'),
    auth.signature
    )

#Proyectos o servicios
DB('project',
    Field('id_project',label='Proyecto id'),
    Field('project_title',label='Titulo'),
    Field('project_img','upload',requires=IS_IMAGE(),autodelete=True),
    Field('project_description',label='Descripcion'),
    Field('client',label='Cliente'),
    Field('fecha','datetime',label='Fecha'),
    auth.signature
)

